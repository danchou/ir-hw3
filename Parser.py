from HTMLParser import HTMLParser

class CleanTextParser(HTMLParser):

    '''
    use one CleanTextParser for a html.
    '''
    def __init__(self):
        HTMLParser.__init__(self)
        self.clean = False
        self.clean_this = False
        self.lst = []

    def handle_starttag(self, tag, attrs):
        if tag=='body': self.clean = True
        if self.clean:
            self.clean_this = not (tag=='link' or tag=='script')
            
    def handle_endtag(self, tag):
        pass

    def handle_data(self, data):
        d = data.strip()
        if self.clean_this and d:  self.lst.append(d)

    def feed(self, html):
        HTMLParser.feed(self, html)
        return ' '.join(self.lst)

def test():
    html = '''
    <html>
    <head>
        <title>Test Html</title>
    </head>

    <body>
        <h1>Hello</h1>
        <div>World
            <p>Information Retrieval</p>
        </div>

        <script> function() {} </script>
        <link href='asddfa.js' />
        <script> function() {} </script>
        <link href='asddfa.js' />

        <h2>Vigil Pavlu</h2>
        <a href="../cathen/15732a.htm">Yakima Indians</a> - A Shahaptian tribe formerly dwelling on the banks of the Columbia, the Wenatchee, and northern branches of the Yakima Rivers, in the east of Washington<!--t8=93--><br>
    </body>
    </html>
    '''

    html2 = '''
    <html>
    <head>
        <title>Test Html</title>
    </head>

    <body>
        <a href="../cathen/15732a.htm">Yakima Indians</a> - A Shahaptian tribe formerly dwelling on the banks of the Columbia, the Wenatchee, and northern branches of the Yakima Rivers, in the east of Washington<!--t8=93--><br>
    </body>
    </html>
    '''
    parser = CleanTextParser()
    print parser.feed(html)

if __name__=='__main__':
    test()
