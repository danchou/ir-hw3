#!/usr/bin/python

import sys
from Parser import CleanTextParser

def parse_file(fname):
    f = open(fname)
    buf = ''
    lst = []
    while not buf.startswith('#####'):
        lst.append(buf)
        buf = f.readline().decode('utf-8')
    header = u''.join(lst)

    buf = f.readline().decode('utf-8')
    lst = []
    while buf:
        lst.append(buf)
        buf = f.readline().decode('utf-8')
    f.close()
    rawBody = u''.join(lst)
    cleanBody = CleanTextParser().feed(rawBody)
    return header, rawBody, cleanBody

def parse_html_body(body):
    for t in soup.findAll(text=True):
        print t

def main():
    fname = sys.argv[1]
    print sys.argv
    header, rawBody, body = parse_file(fname)
    print body


if __name__ == '__main__':
    main()
