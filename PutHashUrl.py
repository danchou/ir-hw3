import sys
import os
from elasticsearch import Elasticsearch
from ParseRawPage import parse_file

#usage: url_hash outDegreeFileDir0, outDegreeFileDir1


SUFNAME = '/Graph/'
es = Elasticsearch()

def load_hash_url_pair(hashname):
    dic = {}
    ret = []
    with open(hashname) as f:
        for line in f:
            line = line.strip()
            i = 0
            while line[i]!=' ': i += 1
            ret.append( (line[0:i], line[i+1:]) )
    return ret

url_hash = load_hash_url_pair(sys.argv[1])

def print_hash_url(dnames):
    visited = set()
    for dname in dnames:
        dname += SUFNAME
        for fname in os.listdir("./"+dname):
            if fname.startswith('out'):
                process(os.path.join(dname,fname), visited)

def process(fpath, visited):
    f = open(fpath)
    while True:
        line = f.readline().strip()
        if not line: break;
        pair = line.split(',')
        hsh = pair[0]
        url = pair[1]
        
        f.readline()
        if hsh in visited:
            continue

        print hsh, url
        visited.add(hsh)
    f.close()

def index(dname):
    i = 0
    for pair in url_hash:
        hsh = pair[0]
        url = pair[1]
        print i, hsh, url

        fname = hsh
        header, raw, text = parse_file(os.path.join(dname, fname))
        doc = {'url':url, 'header':header, 'text':text, 'raw':raw}
        es.index(index='church', doc_type='document', id=int(fname), body=doc)
        i += 1

def hash_url():
    print_hash_url(sys.argv[2:])

def indexing():
    index(sys.argv[2])

if __name__ == '__main__':
    indexing()
