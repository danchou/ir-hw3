package neu.zhoupeng.ir.hw3;

import static neu.zhoupeng.ir.hw3.Utils.CANON;
import static neu.zhoupeng.ir.hw3.Utils.hash32;
import io.mola.galimatias.GalimatiasParseException;
import io.mola.galimatias.URL;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class CrawlEngine {
	
	/**
	 * Seed url that haven't been canonicalized.
	 */
	private final List<String> mSeeds;

	/**
	 * A frontier that contains un-canonicalized urls.
	 */
	private final List<URL> mFrontier;
	
	/**
	 * Pages that already visited.
	 */
	private final Map<Integer, Boolean> mVisited;
	
	/**
	 * A map of time each host is visited. <HostName, Time>
	 */
	private final Map<String, Long> mTimeStamp;
	
	/**
	 * A map of host -> robots rule 
	 */
	private final RobotsMap mRobotsMap;
	
	/**
	 * A atomic counter for number of pages currently haven been crawled.
	 */
	private AtomicInteger mCounter;

	public CrawlEngine(String seed) {
		this(new ArrayList<>(Arrays.asList(seed)));
	}

	public CrawlEngine(List<String> los) {
		mSeeds = los;
		mFrontier = Collections.synchronizedList(new LinkedList<>());
		mVisited = new ConcurrentHashMap<>();
		mTimeStamp = new ConcurrentHashMap<>();
		mRobotsMap = new RobotsMap();
		mCounter = new AtomicInteger(0);
	}
	/**
	 * Start crawling.
	 */
	public void crawl(int cores)  {
		long start = System.currentTimeMillis();
		try {
			for ( String seed : mSeeds ) {
				URL c = CANON.canonicalize(URL.parse(seed));
				mFrontier.add(c);
				mVisited.put(hash32(c.toString()), true);
			}
		} catch (GalimatiasParseException e) {
			System.err.println("seeding error!");
			return;
		}
		
		Thread[] ts = new Thread[cores];
		for (int i=0; i<cores; i++) {
			ts[i] = new Thread(new Crawler(mFrontier, mVisited, 
					mTimeStamp, mCounter, mRobotsMap, i));
			ts[i].start();
		}

		try {
			for (int i = 0; i < cores; i++)
				ts[i].join();
		} catch (InterruptedException e) {
			System.err.println("Thread Interrupted during crawling");
		}
		
		System.out.println("Duration: " + (System.currentTimeMillis()-start));
	}
	
	public static void main(String[] args) {
		CrawlEngine ce = new CrawlEngine(Settings.SEEDS);
		ce.crawl(Settings.THREADCOUNT);
	}
}
