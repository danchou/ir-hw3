package neu.zhoupeng.ir.hw3;

import io.mola.galimatias.GalimatiasParseException;
import io.mola.galimatias.URL;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import javax.sql.rowset.spi.SyncResolver;

import org.apache.commons.io.FileUtils;

import crawlercommons.robots.BaseRobotRules;
import static neu.zhoupeng.ir.hw3.Utils.*;

public class Crawler implements Runnable {

	private final Map<Integer, Boolean> mSharedVisited;
	private final Map<String, Long> mSharedTimeStamp;
	private final List<URL> mFrontier;
	private final AtomicInteger mSharedCounter;
	
	private final PrintWriter mOutDegreeWriter;
	
	private final RobotsMap mSharedMap;
	
	public Crawler(List<URL> frontier,
			Map<Integer, Boolean> visited,
			Map<String, Long> timestamp,
			AtomicInteger counter,
			RobotsMap map,
			int id) {
		mSharedVisited = visited;
		mSharedTimeStamp = timestamp;
		mFrontier = frontier;
		mSharedCounter = counter;
		mSharedMap = map;
		
		try {
			mOutDegreeWriter = new PrintWriter(new File(Settings.OutDegreePath+id));
		} catch (FileNotFoundException e) {
			throw new RuntimeException("OutDegreeFile CANNOT be WRITTEN!");
		}
	}

	@Override
	public void run() {
		try {
			crawl();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void crawl() throws InterruptedException {
		boolean extractUrl = true;
		while ( true ) {
			URL head = null;
			String host = null;
			long current_time = -1;
			
			synchronized (mFrontier) {
				Iterator<URL> iter = mFrontier.iterator();
				while ( iter.hasNext() ) {
					URL cur = iter.next();
					host = cur.host().toString();
					Long lastTimeVisited = mSharedTimeStamp.get(host);
					current_time = System.currentTimeMillis();
//					System.out.println(cur + ":"+ Thread.currentThread().getId()+","+lastTimeVisited+","+current_time);
					if ( lastTimeVisited==null || current_time-lastTimeVisited>=1000) {
//						System.out.println("***: " + cur + ", " + lastTimeVisited + ","+ current_time);
						head = cur;
						if (mFrontier.size()>Settings.THRESHOLD) extractUrl=false;
						iter.remove();
						mSharedTimeStamp.put(host, current_time);
						break;
					}
				}
			}

			if ( head==null ) {
				Thread.sleep(200);
				continue;
			}
			
			/* check robots.txt. */
			BaseRobotRules rule = mSharedMap.get(host);
			if (rule==null) {
				synchronized(mSharedMap) {
					if ( rule==null) {
						rule = createRobotRules("http://" + host);
						mSharedMap.put(host, rule);
					}
				}
			}
			
			final String curPageUrl = head.toString();
			if ( !rule.isAllowed(curPageUrl) ) continue;
			
			Object[] res = crawlPage(curPageUrl);
			if ( res==null ) continue;
			
			@SuppressWarnings("unchecked")
			List<String> newUrls = (ArrayList<String>)res[0];
			String rawBody = (String)res[1];

			String curPageUrlHash = String.valueOf(hash32(curPageUrl));
			System.out.println(mFrontier.size() +"/" +  Thread.currentThread().getId() + "/" +
					curPageUrlHash + "/"+ curPageUrl);
			try {
				FileUtils.writeStringToFile(new File(Settings.rawPage(curPageUrlHash)), rawBody);
			} catch (IOException e1) {
				System.err.println("failed to save " + Settings.rawPage(curPageUrlHash));
				continue;
			}
			
			if ( mSharedCounter.incrementAndGet()>Settings.TOTALPAGES) break;
			// add new urls to mSharedVisited
			Set<Integer> urlsOnThisPage = new HashSet<>();
			List<String> outlinks = new LinkedList<>();
			for ( String surl : newUrls ) {
				URL u = null;
				surl = local2GlobalUrl(head, surl);
				try {
					u = CANON.canonicalize( URL.parse(surl) );
				} catch (GalimatiasParseException e) {
					e.printStackTrace();
					continue;
				}
				Integer hash = hash32(u.toString());
				if ( !urlsOnThisPage.contains(hash) ) {
					outlinks.add(String.valueOf(hash));
					urlsOnThisPage.add(hash);
				}
				if (extractUrl && !mSharedVisited.containsKey(hash)) {
					mSharedVisited.put(hash, true);
					mFrontier.add(u);
				}
				// write to degree file
			}
			mOutDegreeWriter.printf("%s,%s\n%s\n",
					curPageUrlHash, curPageUrl, String.join(",", outlinks));
		} /* end of while.*/
		
		mOutDegreeWriter.flush();
		mOutDegreeWriter.close();
		
		System.out.println(Thread.currentThread().getId() + " IS DONE!");
	} /* end of function.*/

}
