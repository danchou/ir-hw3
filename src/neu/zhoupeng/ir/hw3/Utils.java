package neu.zhoupeng.ir.hw3;

import io.mola.galimatias.GalimatiasParseException;
import io.mola.galimatias.canonicalize.RFC2396Canonicalizer;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;

import crawlercommons.fetcher.http.BaseHttpFetcher;
import crawlercommons.fetcher.http.UserAgent;
import crawlercommons.robots.BaseRobotRules;
import crawlercommons.robots.BaseRobotsParser;
import crawlercommons.robots.RobotUtils;
import crawlercommons.robots.SimpleRobotRulesParser;

/**
 * Hello world!
 */
public class Utils {
	/**
	 * Canonicalizer
	 */
	public static final RFC2396Canonicalizer CANON = new RFC2396Canonicalizer();
	
	private static final HashFunction MURMUR32 = Hashing.murmur3_32();
	
	public static String[] splitUrl(String surl) {
		URL url = null;
		try {
			if ( !surl.startsWith("http") ) {
				surl = "http://" + surl;
			}
			url = new URL(surl);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		}
		return new String[]{url.getProtocol(), url.getHost().toLowerCase(),
				url.getPath(), url.getQuery()};
	}

	public static String assembleUrl(String[] params) {
		if ( params==null ) return null;

		try {
			System.out.println(params[0] + "," + params[1]);
			URI uri = new URI(params[0], params[1], params[2], params[3]).normalize();
			return uri.toString();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String canonicalize(String surl) {
		return assembleUrl( splitUrl(surl) );
	}

	public static List<String> discoverLinks(String body) {
		List<String> lstUrls = new ArrayList<>();
		Element bodyElem = Jsoup.parse(body).body();
		if ( bodyElem==null ) return lstUrls;

		for ( Element e : bodyElem.select("*") ) {
			String tagName = e.tagName();
			if ( !"script".equals(tagName) && !"link".equals(tagName)
					&& "a".equals(tagName) ){
					lstUrls.add( e.attr("href") );
			}
		}
		return lstUrls;
	}
	
	public static String parseHtmlBody(String body, List<String> lstUrls) {
		Document doc = Jsoup.parse(body);
		StringBuffer sb = new StringBuffer();
		for ( Element e : doc.body().select("*") ) {
			String tagName = e.tagName(), ownText = e.ownText();
			if ( !"script".equals(tagName) && !"link".equals(tagName)
					&& !"".equals(ownText) ) {
				if ( "a".equals(tagName) )
					lstUrls.add( e.attr("href") );
				sb.append(ownText + " ");
			}
		}
		return sb.toString();
	}

	/**
	 * @param surl a Canonicalized url
	 * @return a ParseResult crawled from this page.
	 */
	/*
	public static ParseResult parsePage(String surl) {
		// canonicalize
		List<String> rets = new ArrayList<>();
		if ( surl == null ) return null;
		
		URL url = null; String body = null;
		try {
			url = new URL(surl);
			URLConnection con = url.openConnection();
			// verify the type of page. Crawl only text/html, 
			String contenttype = con.getHeaderField("Content-Type");
			if ( !contenttype.contains("text/html") ) return null;
			
			InputStream in = con.getInputStream();
			String encoding = con.getContentEncoding();
			encoding = encoding == null ? "UTF-8" : encoding;
			body = parseHtmlBody(IOUtils.toString(in, encoding), rets);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return new ParseResult(surl, rets, body);
	}*/
	
	/**
	 * @param surl a canonicalized url
	 * @return a list of newly discovered urls and raw html body.
	 */
	public static Object[] crawlPage(String surl) {
		if ( surl == null ) return null;
		
		try {
			URL url = new URL(surl);
			HttpURLConnection con = (HttpURLConnection)url.openConnection();
			// verify the type of page. Crawl only text/html, 
			String contenttype = con.getHeaderField("Content-Type");
			String contentlang = con.getHeaderField("Content-language");
			String encoding = con.getContentEncoding();
			if ( con.getResponseCode()!=200 ||
					contenttype==null || !contenttype.contains("text/html") ||
					(contentlang!=null && !contentlang.contains("en")) ||
					(encoding!=null && !encoding.contains("UTF-8")))
				return null;
			
			InputStream in = con.getInputStream();
			
			String header = String.join("\n", 
					con.getHeaderFields().entrySet()
					.stream()
					.map(	entry -> {
						String key = entry.getKey()==null?"":entry.getKey()+": ";
						return key+String.join("", entry.getValue()); 
					})
					.collect(Collectors.toList()));
			String body = header + "\n#####\n" +  IOUtils.toString(in, "UTF-8");
			
			List<String> rets = discoverLinks(body);
			return new Object[]{rets, body};
		} catch (IOException  e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void testJsoup() throws IOException {
		Document doc = Jsoup.parse(new File("src/test/data/sample1.html"), "UTF-8");
		StringBuffer sb = new StringBuffer();
		for ( Element e : doc.body().select("*") ) {
			String tagName = e.tagName();
			String ownText = e.ownText();
			if ( !"script".equals(tagName) && !"link".equals(tagName)) {
				if ( "a".equals(tagName) ) {
					System.out.println( e.attr("href") );
				}
				sb.append(ownText);
			}
		}
	}

	public static String local2GlobalUrl (io.mola.galimatias.URL url, String localurl) {

		// remove # tag
		int tail = localurl.length();
		for (int i=localurl.length()-1; i>=0; i--) {
			if ( localurl.charAt(i)=='#' || localurl.charAt(i)=='?' ) {
				tail = i;
			}
		}
		localurl = localurl.substring(0, tail);

		if ( localurl.startsWith("http") ) return localurl; 
		if ( localurl.startsWith("//") ) return "http:" + localurl;
		if ( localurl.startsWith("/") )
			return "http://" + url.host().toString() + localurl;

		String urlThisPage = url.toString();
		
		int i = urlThisPage.length() - 1;
		while (i >= 0 && urlThisPage.charAt(i) != '/') i--;
		if (i == -1) return urlThisPage + "/" + localurl;
		return urlThisPage.substring(0, i + 1) + localurl;
	}

	private static void test_crawlpage() {
		Object[] obj = crawlPage("http://en.wikipedia.org/wiki/Belarusian_Greek_Catholic_Church");
		List<String> localUrls =  (ArrayList<String>)obj[0];
		for ( String s : localUrls) {
			try {
				System.out.println(local2GlobalUrl(CANON.canonicalize(io.mola.galimatias.URL.parse("http://en.wikipedia.org")), s));
			} catch (GalimatiasParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

    public static void main( String[] args ) {
    	test_crawlpage();
    }

    public static int hash32(String url) {
    	return MURMUR32.hashString(url, Charset.forName("UTF-8")).asInt();
    }
    
    private static final BaseRobotsParser parser = new SimpleRobotRulesParser();
    private static final UserAgent agent = new UserAgent("*", "", "");
    private static final BaseHttpFetcher fetcher = RobotUtils.createFetcher(agent ,1);
    public static BaseRobotRules createRobotRules(String host) {
    	try {
			return RobotUtils.getRobotRules(fetcher, parser, new URL(host+"/robots.txt"));
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		}
    }

}

class ParseResult {
	String url;
	List<String> newDiscoveredUrl;
	String trimmedBody;
	
	ParseResult(String url, List<String> newDiscoveredUrl, String trimmedBody) {
		this.url = url;
		this.newDiscoveredUrl = newDiscoveredUrl;
		this.trimmedBody = trimmedBody; 
	}
}