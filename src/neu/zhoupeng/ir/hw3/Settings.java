package neu.zhoupeng.ir.hw3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Settings {
	public final static List<String> SEEDS = new ArrayList<>(
			Arrays.asList("http://en.wikipedia.org/wiki/Catholic_Church",
					"http://en.wikipedia.org/wiki/Christianity",
					"http://en.wikipedia.org/wiki/Catholic_doctrine_regarding_the_Ten_Commandments")
	);
	
	public final static int TOTALPAGES  = 20_000;
	
	public final static String RAWPAGEDIR = "data/rawPages/";
	
	public static String rawPage(String fname) {
		return RAWPAGEDIR + fname;
	}
	
	public final static String OutDegreePath = "data/outDegreeFile";
	
	public final static int THRESHOLD = 60_000;
	
	public final static int THREADCOUNT = 8;

}
