package neu.zhoupeng.ir.hw3;

public class ArrayHeap<T extends HeadNode> {

	/**
	 * Buffer for data.
	 */
	private T[] mData;

	/**
	 * Number of elements in the heap.
	 */
	private int mSize;
	
	public ArrayHeap() {
		this(0);
	}
	/**
	 * @param initialsize Initial capacity for data buffer. This
	 * is not the size of the heap.
	 */
	@SuppressWarnings("unchecked")
	public ArrayHeap(int initialsize) {
		mData = (T[]) new Object[initialsize];
		mSize = 0;
	}
	
	public void addKey(T node) {
		mData[mSize] = node;
		increaseKey(mSize, 0);
		
		++mSize;
	}

	public final void increaseKey(int idx, int incr) {
		rangeCheck(idx);
		
		T obj = mData[idx];
		obj.incr(incr);
		int pid = (idx-1) >> 1;
		T parent = mData[pid];
		while ( parent.compareTo(obj)>0 ) {
			mData[pid] = obj;
			mData[idx] = parent;

			obj = parent;
			idx = pid; pid = (idx-1) >> 1;
			parent = mData[pid];
		}
	}
	
	public T remove(int idx) {
		rangeCheck(idx);
		
		T ret = mData[idx];
		mData[idx] = mData[mSize];
		heapify(idx);
		
		--mSize;
		return ret;
	}

	private void heapify(int idx) {
		while ( true ) {
			int lidx = (idx<<1) + 1;
			int ridx = (idx<<1) + 2;
			T p = mData[idx];
			
			int max = idx;
			if (lidx<mSize && mData[lidx].compareTo(p)<0) max = lidx; 
			if (ridx<mSize && mData[ridx].compareTo(p)<0) max = ridx;
			if ( max==idx) break;
			
			mData[idx] = mData[max]; 
			mData[max] = p;
			idx = max;
		}
	}

	private void rangeCheck(int idx) {
		if ( idx>=mSize || idx<0 ) 
			throw new IndexOutOfBoundsException();
	}

	private void ensureCapacity(int minCapacity) {
		
	}
}
