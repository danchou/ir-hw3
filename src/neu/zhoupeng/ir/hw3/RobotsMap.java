package neu.zhoupeng.ir.hw3;

import java.util.HashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import crawlercommons.robots.BaseRobotRules;

public class RobotsMap {

	private final HashMap<String, BaseRobotRules> mMap;
	private final ReadWriteLock mReadWriteLock;
	private final Lock mReadLock;
	private final Lock mWriteLock; 
	
	public RobotsMap() {
		mMap = new HashMap<>();
		mReadWriteLock = new ReentrantReadWriteLock();
		mReadLock = mReadWriteLock.readLock();
		mWriteLock = mReadWriteLock.writeLock();
	}
	
	public BaseRobotRules get(String host) {
		mReadLock.lock();
		try {
			return mMap.get(host);
		} finally {
			mReadLock.unlock();
		}
	}
	
	public void put(String host, BaseRobotRules rules) {
		mWriteLock.lock();
		try {
			mMap.put(host, rules);
		} finally {
			mWriteLock.unlock();
		}
	}
}
