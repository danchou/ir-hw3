#!/usr/bin/python

import sys
import os

'''
graph : {
    id0 : {
        in : set(),
        out : set() 
    },
    id1 : {}
    ... ...
    idn : {}
}
'''
SUFNAME = '/Graph/'
def getAllNode(dnames):
    graph = {}
    for dname in dnames:
        dname += SUFNAME
        for fname in os.listdir("./"+dname):
            if fname.startswith('out'):
                process(os.path.join(dname,fname), graph)

    for key, value in graph.iteritems():
        print key,'#', len(value['in']), '#', len(value['out'])
 
def process(fpath, graph):
    f = open(fpath)
    while True:
        line = f.readline()
        if not line: break;
        hsh = line.split(',')[0]

        line = f.readline().strip()
        if line: outlinks = set(line.split(','))

        if not hsh in graph:
            graph[hsh] = {"in":set(), "out":set()}
        graph[hsh]['out'].update(outlinks)
 
        for link in outlinks:
            if not link in graph:
                graph[link] = {'in':set(), 'out':set()}
            graph[link]['in'].add(hsh)

    f.close()

def test_process():
    samplefpath = '/home/zhoupeng/workspace/InfoRetrieval/hw3/data/' +\
                    'sampleOutDegreeFile'
    graph = {}
    process(samplefpath, graph)
    count = 0
    for key, value in graph.iteritems():
        print key, len(value['in']), len(value['out'])
    print count

def main():
    dnames = sys.argv[1:]
    getAllNode(dnames)

if __name__ == '__main__':
    main()

