package neu.zhoupeng.ir.hw3;

import static neu.zhoupeng.ir.hw3.Utils.*;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import crawlercommons.robots.BaseRobotRules;

/**
 * Unit test for simple App.
 */
public class UtilsTest {
	
	public void test_canonicalize() {
		assertEquals("http://david.choffnes.com/classes/cs4700sp15/",
				canonicalize("HTTP://david.choFFnes.com/classes/cs4700sp15/"));
		
		assertEquals("http://www.example.com/SomeFile.html",
				canonicalize("HTTP://www.Example.com/SomeFile.html"));
		
		assertEquals("http://www.example.com",
				canonicalize("http://www.example.com:80	"));
		
		assertEquals("http://www.example.com/a.html",
				canonicalize("http://www.example.com/a.html#anything"));
		
		assertEquals("http://www.example.com/a.html",
				canonicalize("http://www.example.com///a.html"));
	}

	public void debug() throws FileNotFoundException {
		Scanner sc = new Scanner(new File("test/data/err.http"));
		while (sc.hasNextLine() ) {
			String surl =  sc.nextLine();
			System.out.println(surl);
			crawlPage(surl);
		}
		sc.close();
	}
	
	@Test
	public void debug_discover_link() throws IOException {
		String body = FileUtils.readFileToString(new File("test/data/errbody.html"));
		discoverLinks(body);
	}
	
	@Test
	public void test_hash32() {
		System.out.println( hash32("http://en.wikipedia.org/wiki/Catholic_Church") );
		System.out.println( hash32("http://en.wikipedia.org/wiki/Christianity") );
		System.out.println( hash32("http://en.wikipedia.org/wiki/Catholic_doctrine_regarding_the_Ten_Commandments"));
	}
	
	@Test
	public void test_rules() {
		BaseRobotRules rules = createRobotRules("http://www.sissna.com");
		System.out.println( rules.isAllowed("http://wikipedia.com/fuck/asdf") );
	}
}
